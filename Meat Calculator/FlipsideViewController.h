//
//  FlipsideViewController.h
//  Meat Calculator
//
//  Created by Lenny Khazan on 12/2/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Meat.h"

@class FlipsideViewController;

@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
- (void)temperatureUnitsChangedTo:(TemperatureUnits)newTemperatureUnits;
- (void)weightUnitsChangedTo:(WeightUnits)newWeightUnits;
@end

@interface FlipsideViewController : UIViewController

@property (weak, nonatomic) IBOutlet id <FlipsideViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *weightSelector;
@property (weak, nonatomic) IBOutlet UISegmentedControl *temperatureSelector;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

- (IBAction)done:(id)sender;
- (void)setWeightUnitsTo:(WeightUnits)weightUnits;
- (void)setTemperatureUnitsTo:(TemperatureUnits)temperatureUnits;

@end
