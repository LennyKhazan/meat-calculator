//
//  PropertyListParser.h
//  Meat Calculator
//
//  Created by Lenny Khazan on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PropertyListParser : NSObject

+(id)parsePropertyListWithFilename:(NSString *)filename;

@end
