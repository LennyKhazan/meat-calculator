//
//  PropertyListParser.m
//  Meat Calculator
//
//  Created by Lenny Khazan on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PropertyListParser.h"

@implementation PropertyListParser

+(id)parsePropertyListWithFilename:(NSString *)filename {
    NSData *plistData;  
    NSString *error;  
    NSPropertyListFormat format;  
    id plist;  
    
    NSString *localizedPath = [[NSBundle mainBundle] pathForResource:[filename stringByReplacingOccurrencesOfString:@".plist" withString:@""] ofType:@"plist"];  
    plistData = [NSData dataWithContentsOfFile:localizedPath];   
    
    plist = [NSPropertyListSerialization propertyListFromData:plistData mutabilityOption:NSPropertyListImmutable format:&format errorDescription:&error];  
    if (!plist) {  
        UIAlertView *parseErrorAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"An error occurred. Please contact us at meatcalculatorapp@gmail.com for assistance." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [parseErrorAlertView show];
    }  
    
    return plist;
}

@end
