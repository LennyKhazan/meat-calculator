//
//  MeatCalculatorViewController.m
//  Meat Calculator
//
//  Created by Lenny Khazan on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MeatCalculatorViewController.h"

#define userInterfaceIdiomIsiPhone() [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone

const NSString *kEmptyFieldPlaceholder = @"--";

@interface MeatCalculatorViewController()
    
@property (strong, nonatomic) NSTimer *cookTimer;

-(void)temperatureUnitsChangedTo:(TemperatureUnits)newTemperatureUnits;

@end

@implementation MeatCalculatorViewController

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/******************************************************/ // /*************************************************************************/
/******************************************************/ // /*************************************************************************/
/**    kDebugMode must be set to NO for release!!    **/ // /*************************************************************************/
/**                 SUPER IMPORTANT!                 **/ // /**                                                                     **/
/******************************************************/ // /**  ***     ****   *       *   ****   *       *   ***    ****   ***    **/
/******************************************************/ // /**  *  *    *      * *   * *   *      * *   * *   *  *   *      *  *   **/
//////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!////////// // /**  *  *    *      *  * *  *   *      *  * *  *   *  *   *      *  *   **/
/*********!*/ const BOOL kDebugMode = YES; /*!*********/ // /**  ***     ***    *   *   *   ***    *   *   *   ***    ***    ***    **/
//////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!////////// // /**  * *     *      *       *   *      *       *   *  *   *      * *    **/
/******************************************************/ // /**  *  *    *      *       *   *      *       *   *  *   *      *  *   **/
/******************************************************/ // /**  *   *   ****   *       *   ****   *       *   ***    ****   *   *  **/
/**    kDebugMode must be set to NO for release!!    **/ // /**                                                                     **/
/**                 SUPER IMPORTANT!                 **/ // /*************************************************************************/
/******************************************************/ // /*************************************************************************/
/******************************************************/ // /*************************************************************************/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@synthesize flipsidePopoverController = _flipsidePopoverController;
@synthesize flipside = _flipside;
@synthesize weightTextField = _weightTextField;
@synthesize internalTemperatureLabel = _internalTemperatureLabel;
@synthesize internalTemperatureUnitLabel = _internalTemperatureUnitLabel;
@synthesize cookTimeLabel = _cookTimeLabel;
@synthesize ovenTemperatureLabel = _ovenTemperatureLabel;
@synthesize ovenTemperatureUnitLabel = _ovenTemperatureUnitLabel;
@synthesize restTimeLabel = _restTimeLabel;
@synthesize cookMethodLabel = _cookMethodLabel;
@synthesize meatPickerView = _meatPickerView;
@synthesize hideKeyboardButton = _hideKeyboardButton;
@synthesize model = _model;
@synthesize currentlySelectedMeat = _currentlySelectedMeat;
@synthesize currentlySelectedSubmeat = _currentlySelectedSubmeat;
@synthesize remainingTimeLabel = _remainingTimeLabel;
@synthesize startTimeButton = _startTimeButton;
@synthesize progressView = _progressView;
@synthesize remainingTime = _remainingTime;
@synthesize cookTimer = _cookTimer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(NSString *)currentlySelectedMeat {
    _currentlySelectedMeat = [self.model.meats.allKeys objectAtIndex:[self.meatPickerView selectedRowInComponent:0]];
    return _currentlySelectedMeat;
}

-(Meat *)currentlySelectedSubmeat {
    _currentlySelectedSubmeat = [[self.model.meats objectForKey:self.currentlySelectedMeat] objectAtIndex:[self.meatPickerView selectedRowInComponent:1]];
    return _currentlySelectedSubmeat;
}

-(void)temperatureUnitsChangedTo:(TemperatureUnits)newTemperatureUnits {
    for (NSArray *submeatArray in self.model.meats.allValues) {
        for (Meat *meat in submeatArray) {
            [meat convertTemperatureValuesTo:newTemperatureUnits];
        }
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:newTemperatureUnits forKey:@"MeatCalculatorPrefferedTemperatureUnits"];
    [defaults synchronize];
    
    [self calculateButtonPressed];
}

-(void)weightUnitsChangedTo:(WeightUnits)newWeightUnits {
    for (NSArray *submeatArray in self.model.meats.allValues) {
        for (Meat *meat in submeatArray) {
            [meat convertWeightValuesTo:newWeightUnits];
        }
    }
    //this should probably be in a helped method
    if (newWeightUnits == pounds)
        self.weightTextField.placeholder = @"pounds";
    else if (newWeightUnits == grams)
        self.weightTextField.placeholder = @"grams";
    else if (newWeightUnits == ounces)
        self.weightTextField.placeholder = @"ounces";
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:newWeightUnits forKey:@"MeatCalculatorPrefferedWeightUnits"];
    [defaults synchronize];    
    self.weightTextField.text = @"";
    [self calculateButtonPressed];
}

#pragma mark - UIPickerViewDelegate / UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 2;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *retval = (id)view;
    if (!retval) {
        retval= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)];
    }
    if (component == 0) {
        retval.text = [self.model.meats.allKeys objectAtIndex:row];
    } else {
        retval.text = [[[self.model.meats objectForKey:self.currentlySelectedMeat] objectAtIndex:row] meat];
    }
    retval.font = [UIFont systemFontOfSize:15];
    retval.backgroundColor = [UIColor clearColor];
    retval.text = [@" " stringByAppendingString:retval.text];
    return retval;
}


-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (userInterfaceIdiomIsiPhone()) {
        if (component == 0) {
            return 180;
        } else {
            return 300;
        }
    } else {
        return self.view.frame.size.height / 2;  
    }
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return [self.model.meats.allKeys count];
    } else {
        return [[self.model.meats objectForKey:self.currentlySelectedMeat] count];
    }
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        NSString *title = [[self.model.meats allKeys] objectAtIndex:row];
        return title;
    } else {
        return [[[self.model.meats objectForKey:self.currentlySelectedMeat] objectAtIndex:row] meat];
    }
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        [self.meatPickerView reloadComponent:1];
        [self calculateButtonPressed];
    } else {
        [self calculateButtonPressed];
    }
}

#pragma mark -

- (IBAction)hideKeyboard {
    [self.weightTextField resignFirstResponder];
    [self.hideKeyboardButton removeFromSuperview];
}

- (IBAction)editingBegan {
    [self.view addSubview:self.hideKeyboardButton];
}

- (IBAction)calculateButtonPressed {
    float enteredWeight = [self.weightTextField.text floatValue];
    
    if (!enteredWeight)
        enteredWeight = 0.0;
    
    if (enteredWeight == 0.0 && [self.weightTextField.text length] != 0) {
        UIAlertView *enterWeightErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The number you entered for the weight is invalid. Please enter a valid, non-zero value for the weight and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [enterWeightErrorAlert show];
        self.weightTextField.text = @"";
        return;
    }
    
    self.currentlySelectedSubmeat.weight = enteredWeight;
    
    //set the values of the labels
    
    self.ovenTemperatureLabel.text = [[NSString alloc] initWithFormat:@"%g", roundf(self.currentlySelectedSubmeat.cookTemperature)];
    if (self.currentlySelectedSubmeat.temperatureUnits == Fahrenheit)
        self.ovenTemperatureUnitLabel.text = @"F";
    else if (self.currentlySelectedSubmeat.temperatureUnits == Celsius)
        self.ovenTemperatureUnitLabel.text = @"C";
    else 
        self.ovenTemperatureUnitLabel.text = @"F"; //default value
    
    self.cookTimeLabel.text = [[NSString alloc] initWithFormat:@"%g", roundf(self.currentlySelectedSubmeat.cookTime < 0.5 && self.currentlySelectedSubmeat.cookTime != 0 ? 1 : self.currentlySelectedSubmeat.cookTime)];
    
    self.internalTemperatureLabel.text = [[NSString alloc] initWithFormat:@"%g", roundf(self.currentlySelectedSubmeat.internalTemperature)];
    
    if (self.currentlySelectedSubmeat.temperatureUnits == Fahrenheit)
        self.internalTemperatureUnitLabel.text = @"F";
    else if (self.currentlySelectedSubmeat.temperatureUnits == Celsius)
        self.internalTemperatureUnitLabel.text = @"C";
    else 
        self.internalTemperatureUnitLabel.text = @"F"; //default value
    
    self.cookMethodLabel.text = self.currentlySelectedSubmeat.cookMethod;
    
    self.restTimeLabel.text = [[NSString alloc] initWithFormat:@"%g", roundf(self.currentlySelectedSubmeat.restTime)];
    
    self.remainingTime = [self.cookTimeLabel.text intValue];
    self.remainingTimeLabel.text = self.cookTimeLabel.text;
    
    //check for 0 values
    if ([self.ovenTemperatureLabel.text floatValue] == 0.0 | [self.ovenTemperatureLabel.text floatValue] == (float)(-19.2))
        self.ovenTemperatureLabel.text = @"--";
    if ([self.cookTimeLabel.text floatValue] == 0.0)
        self.cookTimeLabel.text = @"--";
    if ([self.internalTemperatureLabel.text floatValue] == 0.0 | [self.internalTemperatureLabel.text floatValue] == (float)(-19.2))
        self.internalTemperatureLabel.text = @"--";
    if ([self.restTimeLabel.text floatValue] == 0.0)
        self.restTimeLabel.text = @"--";
    if ([self.remainingTimeLabel.text floatValue] == 0.0)
        self.remainingTimeLabel.text = @"--";
    if ([self.internalTemperatureLabel.text floatValue] == -19.2)
        self.internalTemperatureLabel.text = @"--";
    if ([self.ovenTemperatureLabel.text floatValue] == (float)(-19.2))
        self.ovenTemperatureLabel.text = @"--";
}

-(void)timerFired:(NSTimer *)timer {
    self.remainingTime = self.remainingTime - 1;
    if (self.remainingTime <= 0) {
        UIAlertView *timerFinishedAlert = [[UIAlertView alloc] initWithTitle:@"Timer" message:@"The timer is done!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [timerFinishedAlert show];
        [self.cookTimer invalidate];
        self.cookTimer = nil;
        [self.startTimeButton setTitle:@"Start" forState:UIControlStateNormal];
        return;
    }
    self.remainingTimeLabel.text = [[NSString alloc] initWithFormat:@"%d", self.remainingTime];
    
    float totalCookTime = [self.cookTimeLabel.text intValue];
    self.progressView.progress = (totalCookTime - self.remainingTime)/totalCookTime;
}

- (IBAction)timerStartPressed {
    if (!self.cookTimer.isValid) {
        if (kDebugMode)
            self.cookTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
        else
            self.cookTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
        [self.startTimeButton setTitle:@"Cancel" forState:UIControlStateNormal];
    } else {
        self.remainingTime = 0;
        [self.startTimeButton setTitle:@"Start" forState:UIControlStateNormal];
        [self.cookTimer invalidate];
        self.cookTimer = nil;
        self.remainingTimeLabel.text = self.cookTimeLabel.text;
        self.progressView.progress = 0.0;
    }
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.flipside = [[FlipsideViewController alloc] initWithNibName:@"FlipsideViewController" bundle:nil];
    self.flipside.delegate = self;
    
    self.hideKeyboardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.hideKeyboardButton.frame = self.view.frame;
    [self.hideKeyboardButton addTarget:self action:@selector(hideKeyboard) forControlEvents:UIControlEventTouchUpInside];
    
    [self.hideKeyboardButton removeFromSuperview];
    self.model = [[MeatCalculatorModel alloc] init];
    
    [self calculateButtonPressed];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL applicationHasBeenLaunchedBefore = [defaults boolForKey:@"MeatCalculatorNotFirstLaunch"];
    if (!applicationHasBeenLaunchedBefore) {
        //Display a greeting message to the user on first launch
        NSString *introMsg = @"Welcome to Meat Calculator! This app prevents you from undercooking your meat by telling you for how long and at what temperature to cook it at. Many diseases can be prevented with this simple tool! To use it, just select the meat you are cooking, enter the weight, and the correct cooking times will appear! For more information, press the \"info\" on the top left of the screen. You can also change certain preferences by clicking on the \"info\" button.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Welcome!" message:introMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        [defaults setBool:YES forKey:@"MeatCalculatorNotFirstLaunch"];
        [defaults synchronize];
    }
    
    TemperatureUnits prefferedTemperatureUnits = [defaults integerForKey:@"MeatCalculatorPrefferedTemperatureUnits"];
    [self temperatureUnitsChangedTo:prefferedTemperatureUnits];
    WeightUnits prefferedWeightUnits = [defaults integerForKey:@"MeatCalculatorPrefferedWeightUnits"];
    [self weightUnitsChangedTo:prefferedWeightUnits];
}

- (void)viewDidUnload
{
    [self setWeightTextField:nil];
    [self setInternalTemperatureLabel:nil];
    [self setInternalTemperatureUnitLabel:nil];
    [self setCookTimeLabel:nil];
    [self setOvenTemperatureLabel:nil];
    [self setOvenTemperatureUnitLabel:nil];
    [self setRestTimeLabel:nil];
    [self setCookMethodLabel:nil];
    [self setMeatPickerView:nil];
    [self setHideKeyboardButton:nil];
    [self setProgressView:nil];
    [self setRemainingTimeLabel:nil];
    [self setStartTimeButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (UIInterfaceOrientationIsLandscape(interfaceOrientation));
}

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissModalViewControllerAnimated:YES];
    } else {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
    }
}

- (IBAction)showInfo:(id)sender
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.flipside.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentModalViewController:self.flipside animated:YES];
    } else {
        if (!self.flipside) {
            self.flipside = [[FlipsideViewController alloc] initWithNibName:@"FlipsideViewController" bundle:nil];
            self.flipside.delegate = self;            
        }
        if (!self.flipsidePopoverController)
            self.flipsidePopoverController = [[UIPopoverController alloc] initWithContentViewController:self.flipside];
        if ([self.flipsidePopoverController isPopoverVisible]) {
            [self.flipsidePopoverController dismissPopoverAnimated:YES];
        } else {
            self.flipsidePopoverController.popoverContentSize = self.flipside.view.frame.size;
            [self.flipsidePopoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
}

@end