//
//  LKMeat.m
//  Meat Calculator
//
//  Created by Lenny Khazan on 12/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Meat.h"

@interface Meat()

@property (nonatomic, strong) float (^calculateTime)(float cookTime, float weight);
@property (nonatomic) float cookTimeValue; //the value used to calculate the actual cook time

@end

@implementation Meat

@synthesize meat = _meat;
@synthesize internalTemperature = _internalTemperature;
@synthesize cookTemperature = _cookTemperature;
@synthesize cookMethod = _cookMethod;
@synthesize restTime = _restTime;
@synthesize calculateTime = _calculateTime;
@synthesize cookTime = _cookTime;
@synthesize weightUnits = _weightUnits;
@synthesize temperatureUnits = _temperatureUnits;
@synthesize weight = _weight;
@synthesize cookTimeValue = _cookTimeValue;

-(id)init {
    self = [super init];
    if (self) {
        self.meat = nil;
        self.calculateTime = ^float(float cookTime, float weight) {
            return 0.0;
        };
        self.cookTemperature = 0;
        self.internalTemperature = 0.0;
        self.restTime = 0.0;
        self.cookMethod = nil;
        self.cookTime = 0.0;
        self.cookTimeValue = 0.0;
        self.weight = 0.0;
        
        //default temperature & weight units:
        self.weightUnits = pounds;
        self.temperatureUnits = Fahrenheit;
    }
    return self;
}

-(id)initWithName:(NSString *)name {
    self = [self init];
    if (self) {
        self.meat = name;
    }
    return self;
}


-(id)initWithName:(NSString *)name cookTemperature:(float)cookTemperature internalTemperature:(float)internalTemperature restTime:(float)restTime cookMethod:(NSString *)cookMethod cookTimeCalculation:(float(^)(float cookTime, float weight))calculateTime cookTime:(float)cookTime weightUnits:(WeightUnits)weightUnits temperatureUnits:(TemperatureUnits)temperatureUnits {
    self = [self init];
    if (self) {
        self.meat = name;
        self.calculateTime = calculateTime;    
        self.cookTemperature = cookTemperature;
        self.internalTemperature = internalTemperature;
        self.restTime = restTime;
        self.cookMethod = cookMethod;
        self.cookTimeValue = cookTime;
        self.weightUnits = weightUnits;
        self.temperatureUnits = temperatureUnits;
        self.cookTime = 0.0;
        
        //default weight (while nothing is entered)
        self.weight = 0;
    }
    return self;
}

-(void)setWeight:(float)weight {
    _weight = weight;
    
    float newWeight = 0.0; //Used temporarily to convert to lb

    //the following should probably be put in a separate helper method
    if (self.weightUnits == ounces) //convert ounces to pounds
        newWeight = self.weight / 16;
    else if (self.weightUnits == grams) //convert grams to pounds
        newWeight = self.weight / 453.6;
    else
        newWeight = self.weight;
    
    self.cookTime = self.calculateTime(self.cookTimeValue, newWeight);
}

-(void)convertWeightValuesTo:(WeightUnits)newWeightUnits {
    self.weightUnits = newWeightUnits;
}

-(void)convertTemperatureValuesTo:(TemperatureUnits)newTemperatureUnits {
    if (self.temperatureUnits == newTemperatureUnits) {
        return;
    } else if (self.temperatureUnits == Fahrenheit && newTemperatureUnits == Celsius) { 
        self.cookTemperature = 0.6 * (self.cookTemperature - 32);
        self.internalTemperature = 0.6 * (self.internalTemperature - 32);
    } else if (self.temperatureUnits == Celsius && newTemperatureUnits == Fahrenheit) {
        self.cookTemperature = (1.8) * (self.cookTemperature + 32);
        self.internalTemperature = (1.8) * (self.internalTemperature + 32);
    }
    self.temperatureUnits = newTemperatureUnits;
}

-(NSString *)description {
    return [NSString stringWithFormat:@"Name:%@\rCook Temperature:%g\rInternal Temperature:%g\rRest Time:%g\rCook Method:%@\rCook Time Value:%g\rEntered weight:%g\rCalculated cook time:%g", self.meat, self.cookTemperature, self.internalTemperature, self.restTime, self.cookMethod, self.cookTimeValue, self.weight, self.cookTime];
}

@end
