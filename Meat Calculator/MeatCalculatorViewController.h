//
//  MeatCalculatorViewController.h
//  Meat Calculator
//
//  Created by Lenny Khazan on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlipsideViewController.h"
#import "MeatCalculatorModel.h"
#import "Meat.h"

@interface MeatCalculatorViewController : UIViewController <FlipsideViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) UIPopoverController *flipsidePopoverController;
@property (strong, nonatomic) FlipsideViewController *flipside;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;
@property (weak, nonatomic) IBOutlet UILabel *internalTemperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *internalTemperatureUnitLabel;
@property (weak, nonatomic) IBOutlet UILabel *cookTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ovenTemperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *ovenTemperatureUnitLabel;
@property (weak, nonatomic) IBOutlet UILabel *restTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *cookMethodLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *meatPickerView;
@property (strong, nonatomic) UIButton *hideKeyboardButton;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) MeatCalculatorModel *model;
@property (strong, nonatomic) NSString *currentlySelectedMeat;
@property (strong, nonatomic) Meat *currentlySelectedSubmeat;
@property (weak, nonatomic) IBOutlet UILabel *remainingTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *startTimeButton;
@property (nonatomic) int remainingTime;

-(void)timerFired:(NSTimer *)timer;
-(IBAction)calculateButtonPressed;

@end
