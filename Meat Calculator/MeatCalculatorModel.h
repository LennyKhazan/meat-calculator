//
//  MeatCalculatorModel.h
//  Meat Calculator
//
//  Created by Lenny Khazan on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Meat.h"
#import "PropertyListParser.h"

@interface MeatCalculatorModel : NSObject
                                                            //key: (NSString*) general meat (Beef, Ham, etc.)
@property (strong, nonatomic) NSMutableDictionary *meats; //value: (NSArray*) submeats (Unstuffed, Stuffed, etc.)

@end
