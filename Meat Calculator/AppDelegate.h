//
//  AppDelegate.h
//  Meat Calculator
//
//  Created by Lenny Khazan on 12/2/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MeatCalculatorViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MeatCalculatorViewController *mainViewController;

@end
