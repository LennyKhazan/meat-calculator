//
//  FlipsideViewController.m
//  Meat Calculator
//
//  Created by Lenny Khazan on 12/2/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "FlipsideViewController.h"

@implementation FlipsideViewController

@synthesize delegate = _delegate;
@synthesize weightSelector = _weightSelector;
@synthesize temperatureSelector = _temperatureSelector;
@synthesize versionLabel = _versionLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.contentSizeForViewInPopover = CGSizeMake(320.0, 480.0);
    }
    return self;
}
							
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.versionLabel.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    TemperatureUnits prefferedTemperatureUnits = [defaults integerForKey:@"MeatCalculatorPrefferedTemperatureUnits"];
    [self setTemperatureUnitsTo:prefferedTemperatureUnits];
    WeightUnits prefferedWeightUnits = [defaults integerForKey:@"MeatCalculatorPrefferedWeightUnits"];
    [self setWeightUnitsTo:prefferedWeightUnits];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (UIInterfaceOrientationIsLandscape(interfaceOrientation));
}

#pragma mark - Actions
- (IBAction)temperatureUnitsChanged:(UISegmentedControl *)sender {
    TemperatureUnits newUnits = sender.selectedSegmentIndex == 0 ? Fahrenheit : Celsius;
    [self.delegate temperatureUnitsChangedTo:newUnits];
}

- (IBAction)weightUnitsChanged:(UISegmentedControl *)sender {
    WeightUnits newUnits;
    if (sender.selectedSegmentIndex == 0)
        newUnits = pounds;
    else if (sender.selectedSegmentIndex == 1)
        newUnits = ounces;
    else if (sender.selectedSegmentIndex == 2)
        newUnits = grams;
    
    [self.delegate weightUnitsChangedTo:newUnits];
}

- (void)setWeightUnitsTo:(WeightUnits)weightUnits {
    if (weightUnits == pounds)
        self.weightSelector.selectedSegmentIndex = 0;
    else if (weightUnits == ounces)
        self.weightSelector.selectedSegmentIndex = 1;
    else if (weightUnits == grams)
        self.weightSelector.selectedSegmentIndex = 2;
    
    [self.delegate weightUnitsChangedTo:weightUnits];
}
- (void)setTemperatureUnitsTo:(TemperatureUnits)temperatureUnits {
    self.temperatureSelector.selectedSegmentIndex = temperatureUnits == Fahrenheit ? 0 : 1;
    
    [self.delegate temperatureUnitsChangedTo:temperatureUnits];
}

- (IBAction)done:(id)sender
{
    [self.delegate flipsideViewControllerDidFinish:self];
}

@end
