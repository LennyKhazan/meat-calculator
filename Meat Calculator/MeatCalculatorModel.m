//
//  MeatCalculatorModel.m
//  Meat Calculator
//
//  Created by Lenny Khazan on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MeatCalculatorModel.h"

@interface MeatCalculatorModel()

@property (strong, nonatomic) float (^calculateCookTimePerWeightUnit) (float cookTime, float weight);
@property (strong, nonatomic) float (^calculateCookTimePerSide) (float cookTime, float weight);
@property (strong, nonatomic) float (^calculateCookTimeNone) (float cookTime, float weight);
@property (strong, nonatomic) float (^calculateCookTimeTotal) (float cookTime, float weight);

+(Meat *)convertToMeatFromPlistDictionary:(NSDictionary *)plistDictionary;

//Class Method getters
+(float(^)(float cookTime, float weight))calculateCookTimePerWeightUnit;
+(float(^)(float cookTime, float weight))calculateCookTimePerSide;
+(float(^)(float cookTime, float weight))calculateCookTimeNone;
+(float(^)(float cookTime, float weight))calculateCookTimeTotal;

@end

@implementation MeatCalculatorModel

@synthesize meats = _meats;
@synthesize calculateCookTimeNone = _calculateCookTimeNone;
@synthesize calculateCookTimePerWeightUnit =_calculateCookTimePerWeightUnit;
@synthesize calculateCookTimeTotal = _calculateCookTimeTotal;
@synthesize calculateCookTimePerSide = _calculateCookTimePerSide;

+(float(^)(float cookTime, float weight))calculateCookTimePerWeightUnit {
    return ^float(float cookTime, float weight) {
        return cookTime * weight;
    };
}

+(float(^)(float cookTime, float weight))calculateCookTimePerSide {
    return ^float(float cookTime, float weight) {
        return cookTime * 2;
    };

}

+(float(^)(float cookTime, float weight))calculateCookTimeNone {
    return ^float(float cookTime, float weight) {
        return 0;
    };
}

+(float(^)(float cookTime, float weight))calculateCookTimeTotal {
    return ^float(float cookTime, float weight) {
        return cookTime;
    };
}

-(float(^)(float cookTime, float weight))calculateCookTimePerWeightUnit {
    return [MeatCalculatorModel calculateCookTimePerWeightUnit];
}

-(float(^)(float cookTime, float weight))calculateCookTimePerSide {
    return [MeatCalculatorModel calculateCookTimePerSide];
}

-(float(^)(float cookTime, float weight))calculateCookTimeNone {
    return [MeatCalculatorModel calculateCookTimeNone];
}

-(float(^)(float cookTime, float weight))calculateCookTimeTotal {
    return [MeatCalculatorModel calculateCookTimeTotal];
}

-(id)init {
    self = [super init];
    if (self) {
        self.meats = [[NSMutableDictionary alloc] init];
        
        //Because of the plist setup, we know this will be a dictionary.

        NSDictionary *plistData = (NSDictionary *)[PropertyListParser parsePropertyListWithFilename:@"Meats"];
        
        NSArray *meatKeys = [plistData allKeys];
        
        NSMutableArray *submeatValues = [[NSMutableArray alloc] init];
        
        for (id meatKeysKey in meatKeys) {
            //meats (NOT submeats!)
            //  Key: Meat (just ignore it!) 
            //Value: Meats (NSDictionary *) - see below
            NSDictionary *meats = [plistData objectForKey:meatKeysKey];
            for (id meatsKey in meats) {
                //submeats
                //  key: String (name of meat)
                //value: Dictionary (meat info)
                NSDictionary *submeats = [meats objectForKey:meatsKey];
                for (id submeatsKey in submeats) {
                    id submeat = [submeats objectForKey:submeatsKey];
                    if ([submeat isKindOfClass:[NSDictionary class]]) {
                        [submeatValues addObject:[MeatCalculatorModel convertToMeatFromPlistDictionary:submeat]];
                    }
                }
                [self.meats setObject:[submeatValues copy] forKey:meatsKey];
                [submeatValues removeAllObjects];
            }
        }
                
    }
    return self;
}

+(Meat *)convertToMeatFromPlistDictionary:(NSDictionary *)plistDictionary {
    
    float (^calculation)(float cookTime, float weight);
    WeightUnits weightUnits;
    TemperatureUnits temperatureUnits;
        
    if ([[plistDictionary objectForKey:@"CalculationMethod"] isEqualToString:@"CalculateTotal"]) {
        calculation = [MeatCalculatorModel calculateCookTimeTotal];
    } else if ([[plistDictionary objectForKey:@"CalculationMethod"] isEqualToString:@"CalculatePerWeightUnit"]) {
        calculation = [MeatCalculatorModel calculateCookTimePerWeightUnit];
    } else if ([[plistDictionary objectForKey:@"CalculationMethod"] isEqualToString:@"CalculateNone"]) {
        calculation = [MeatCalculatorModel calculateCookTimeNone];
    } else if ([[plistDictionary objectForKey:@"CalculationMethod"] isEqualToString:@"CalculatePerSide"]) {
        calculation = [MeatCalculatorModel calculateCookTimePerSide];
    }
    
    if ([[plistDictionary objectForKey:@"WeightUnits"] isEqualToString:@"pounds"]) {
        weightUnits = pounds;
    } else if ([[plistDictionary objectForKey:@"WeightUnits"] isEqualToString:@"ounces"]) {
        weightUnits = ounces;
    } else if ([[plistDictionary objectForKey:@"WeightUnits"] isEqualToString:@"grams"]) {
        weightUnits = grams;
    }
        
    if ([[plistDictionary objectForKey:@"TemperatureUnits"] isEqualToString:@"Fahrenheit"]) {
        temperatureUnits = Fahrenheit;
    } else if ([[plistDictionary objectForKey:@"WeightUnits"] isEqualToString:@"Celsius"]) {
        temperatureUnits = Celsius;
    }
    
    Meat *returnValue = [[Meat alloc] initWithName:[plistDictionary objectForKey:@"Name"] cookTemperature:[[plistDictionary objectForKey:@"CookTemperature"] floatValue] internalTemperature:[[plistDictionary objectForKey:@"InternalTemperature"] floatValue] restTime:[[plistDictionary objectForKey:@"RestTime"] floatValue] cookMethod:[plistDictionary objectForKey:@"CookMethod"] cookTimeCalculation:calculation cookTime:[[plistDictionary objectForKey:@"CookTime"] floatValue] weightUnits:weightUnits temperatureUnits:temperatureUnits];
    
    return returnValue;
}

@end
