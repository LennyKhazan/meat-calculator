//
//  Meat.h
//  Meat Calculator
//
//  Created by Lenny Khazan on 12/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum WeightUnits {
    grams = 0,
    ounces = 1,
    pounds = 2
} WeightUnits;

typedef enum TemperatureUnits {
    Fahrenheit = 0,
    Celsius = 1
} TemperatureUnits;

@interface Meat : NSObject

@property (nonatomic, strong) NSString *meat;
@property (nonatomic, strong) NSString *cookMethod;
@property (nonatomic) float cookTemperature;
@property (nonatomic) float internalTemperature;
@property (nonatomic) float restTime;
@property (nonatomic) float cookTime; //automatically uses calculateTime to get time based on weight
@property (nonatomic) float weight;

@property (nonatomic) WeightUnits weightUnits;
@property (nonatomic) TemperatureUnits temperatureUnits;

-(id)initWithName:(NSString *)name cookTemperature:(float)cookTemperature internalTemperature:(float)internalTemperature restTime:(float)restTime cookMethod:(NSString *)cookMethod cookTimeCalculation:(float(^)(float cookTime, float weight))calculateTime cookTime:(float)cookTime weightUnits:(WeightUnits)weightUnits temperatureUnits:(TemperatureUnits)temperatureUnits;
-(void)convertWeightValuesTo:(WeightUnits)newWeightUnits;
-(void)convertTemperatureValuesTo:(TemperatureUnits)newTemperatureUnits;

@end
